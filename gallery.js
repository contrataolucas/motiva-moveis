var indexGalerry = 0;

$(window).on("load", function(){
	if($('.gallery').length){
		gallery();
	}	
});

function gallery(){
	var galleryContainer = $('.gallery');
	var galleryList = galleryContainer.find('ul');
	
	galleryList.find('li').each(function(index){
		var it = $(this);
		it.attr('data-index', index);
	});

	resetGallery(galleryList);

	$(window).resize(function(e){
		resetGallery(galleryList);
	});

	galleryActions(galleryContainer);
}

function resetGallery(galleryList){
	var w = $(window).width();
	var widthList = 0;
	var li = galleryList.find('li');
	var width = 0;
	var left = 0;
	indexGalerry = 0;
	
	galleryList.find('li img').css({
		'max-width' : w-20
	});

	galleryList.find('li').each(function(index){
		var it = $(this);
		var widthIt = it.width();
		widthIt = widthIt +200;
		widthList+=widthIt;
	});

	galleryList
	.stop()
	.animate({
		'width' : widthList,
		'left' : 0
	}, function(e){

		li.each(function(index){
			var it = $(this);
			width = it.width();
			left = it.offset().left;
			width = parseInt(width);
			left = parseInt(left);

			it.attr('data-width', width);
			it.attr('data-left', left);
		});
	});
}
function moveGallery(galleryContainer, galleryList, indexAtv, leftPosition, limit){

	if(indexAtv > indexGalerry){
		galleryContainer.find('.content').addClass('trans-left');
	}else{
		galleryContainer.find('.content').addClass('trans-right');
	}

	indexGalerry = indexAtv;
	var atvFigure = galleryList.find('li[data-index='+ indexGalerry +']');

	var leftGallery = galleryList.offset().left;
	var leftIt = atvFigure.offset().left;

	var leftPositionIt = leftIt - leftGallery;

	console.log(leftPositionIt + " ---- " + leftPosition);


	galleryList
	.stop()
	.delay(111)
	.animate({
		'left': -(leftPosition)// - (padding*2))
	}, function(e){
		galleryContainer.find('.content').removeClass('trans-left');
		galleryContainer.find('.content').removeClass('trans-right');
	});

	$('section.gallery h2 strong span').html((indexGalerry + 1) + "/" + (limit + 1));
	//$('section.gallery h2 strong span').html((indexGalerry + 1) + "/" + (limit + 1));

	return false;
}

function galleryActions(galleryContainer){
	
	var galleryList = galleryContainer.find('ul');
	var limit = galleryList.find('li').length-1;

	galleryList.find('li').click(function(e){

		var it = $(this);
		var leftPosition = it.data('left');
		var indexAtv = it.data('index');
		var padding = parseInt($("main").css("padding-left"));

		moveGallery(galleryContainer, galleryList, indexAtv, leftPosition, limit);	
		
	});
	galleryContainer.find('.arrows button').click(function(e){
		var it = $(this);
		var buttonIndex = indexGalerry;

		if(it.hasClass('left')){
			if(buttonIndex>0){
				buttonIndex --;
			}else{
				buttonIndex = limit;
			}
		}else{
			if(buttonIndex<=limit-1){
				buttonIndex ++;
			}else{
				buttonIndex = 0;
			}
		}
		
		var atvFigure = galleryList.find('li[data-index='+ buttonIndex +']');
		var leftPosition = atvFigure.data('left');

		moveGallery(galleryContainer, galleryList, buttonIndex, leftPosition, limit);	

	});

	

	/*galleryContainer.find('.arrows button').click(function(e){
		var it = $(this);
		var buttonIndex = indexGalerry;
		var padding = parseInt($("main").css("padding-left"));
		


		if(it.hasClass('left')){
			if(buttonIndex>0){
				buttonIndex --;
			}else{
				buttonIndex = limit;
			}
		}else{
			if(buttonIndex<=limit-1){
				buttonIndex ++;
			}else{
				buttonIndex = 0;
			}
		}
		
		var atvFigure = galleryList.find('li[data-index='+ buttonIndex +']');
		var leftPosition = atvFigure.data('left');

		if(buttonIndex > indexGalerry){
			galleryContainer.find('.content').addClass('trans-left');
		}else{
			galleryContainer.find('.content').addClass('trans-right');
		}

		indexGalerry = buttonIndex;


		galleryList
		.stop()
		.delay(111)
		.animate({
			'left': -(leftPosition - (padding*2))
		}, function(e){
			galleryContainer.find('.content').removeClass('trans-left');
			galleryContainer.find('.content').removeClass('trans-right');
		});

		$('section.gallery h2 strong span').html((indexGalerry + 1) + "/" + (limit + 1));
	});
	*/
}

